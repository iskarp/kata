// Author: Ilona Skarp 14.1.2022 10:00-11:00

#include<iostream>
#include<string>
int main()
{
    time_t now = time(0);
    tm *ltm = localtime(&now);

    int result = 0;
    int hours = 16 - ltm->tm_hour;
    int minutes = 60 - ltm->tm_min;
    if(hours <= 0) {
            minutes = 0;
        }
    
    if(ltm->tm_wday==0) {
        std::cout << "Go to bed, it's weekend, no work!" <<std::endl;
    }else if (ltm->tm_wday==1) {
        result = 4*8 + hours;
        std::cout << result <<":"<<minutes<<std::endl;
    } else if (ltm->tm_wday==2) {
        result = 3*8 + hours;
        std::cout << result <<":"<<minutes<<std::endl;
    } else if (ltm->tm_wday==3) {
        result = 2*8 + hours;
        std::cout << result <<":"<<minutes<<std::endl;
    } else if (ltm->tm_wday==4) {
        result = 8 + hours;
        std::cout << result <<":"<<minutes<<std::endl;
    }else if (ltm->tm_wday==5) {
        result = hours;
        std::cout << result <<":"<<minutes<<std::endl;
    } else if (ltm->tm_wday==6) {
        std::cout << "Goo to bed, it's weekend!! No work."<<std::endl;
    }
return 0;
}