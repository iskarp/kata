//Ilona Skarp Noroff exersice C++ coding kata for day 17.1.2022 
#include<iostream>
#include <string>
#include <cmath>
int main() {

    std::string cards[52] = {"♠", "♠2","♠3", "♠4", "♠5", "♠6", "♠7", "♠8", "♠9", "♠10", "♠J", "♠Q", "♠K", "♥","♥2", "♥3","♥4","♥5","♥6","♥7", "♥8","♥9","♥10","♥J","♥Q","♥K","♣", "♣2","♣3","♣4","♣5","♣6","♣7","♣8","♣9","♣10","♣J", "♣Q", "♣K", "♦", "♦2","♦3","♦4","♦5","♦6","7♦","♦8","♦9","♦10","♦J","♦Q","♦K"};
    int positions[52] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    int cardCount = 52;
    int randomNumber;
    std::cout << "Playing card deck randomly:";
    
    while(cardCount > 0) {
         randomNumber = rand() % 52 + 1;
         while(positions[randomNumber-1]== true) {
                 randomNumber = rand() % 52 + 1;
         }
         if(randomNumber <=52 && randomNumber > 0) {
            std::cout << cards[randomNumber-1]<<std::endl;
            positions[randomNumber-1] = true;
            cardCount--;  
         }
         do {
                std::cout << '\n' << "Press an enter to continue..."<<std::endl;
            } while (std::cin.get() != '\n'); 
    }
    return 0;
}
