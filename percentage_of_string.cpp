#include<iostream>
#include<string>
#include<cmath>

double percentage_of_string(std::string str1, std::string str2) {

    double hits = 0;
    double result =0;
    for(int i = 0; i < str1.length();i++) {
         for(int j = 0; j < str2.length();j++) {
             if(str1[i] >= 65 && str1[i] <= 90) {
                 str1[i] = str1[i] +32;
             }
             if(str2[j] >= 65 && str2[j] <= 90) {
                 str2[j] = str2[j] +32;
             }
             if(str1[i]== str2[j]) {
                 hits++;
                 break;
             }
         }
    }
    if(hits >0) {
        result = (hits /str1.length())*100;
    }
    return result;
}

int main() {

    std::string str1;
    std::string str2;

    std::cout<<"Give a strinG nro 1"<< std::endl;
    std::cin >>str1,
    std::cout<<"Give a strinG nro 2"<< std::endl;
    std::cin >>str2;
    std::cout << "First string is "<< percentage_of_string(str1, str2) <<"% of second";
}